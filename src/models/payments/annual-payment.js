const Payment = require('./payment');

// Annual payment declaration
class AnnualPayment extends Payment {
    constructor(person, price) {
        super(person, price)
    }

    get total() {
        this._total *= 0.7;
        // Postcondition
        if (this._total < 100) throw new Error('Annual payments are available from 100 dollars');
        return this._total.toFixed(1);
    }
}

module.exports = AnnualPayment;