const Payment = require('./payment');

// Monthly payment declaration
class MonthlyPayment extends Payment {
    constructor(person, price, countMonths) {
        super(person, price);
        this._countMonths = countMonths;
    }

    get countMonths() {
        return this._countMonths;
    }

    set countMonths(value) {
        this._countMonths = value;
    }

    get total() {
        // Precondition
        if (this._countMonths >= 3) {
            this._total *= 0.8;
        }
        return this._total;
    }
}

module.exports = MonthlyPayment;