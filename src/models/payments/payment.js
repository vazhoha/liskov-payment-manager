// Base payment declaration
class Payment {
    constructor(person, price) {
        this._total = person.employeesCount * price;
    }

    get total() {
        return this._total;
    }

    set total(value) {
        this._total = value;
    }
}

module.exports = Payment;