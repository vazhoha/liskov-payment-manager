const Person = require('./person');

// Corporation declaration
class Corporation extends Person {
    constructor(companyName, companyAccount, employeesCount) {
        super(companyName, companyAccount);
        this._employeesCount = employeesCount;
    }

    get employeesCount() {
        return this._employeesCount;
    }

    set employeesCount(value) {
        this._employeesCount = value;
    }
}

module.exports = Corporation;
