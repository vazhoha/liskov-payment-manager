const Person = require('./person');

// Private person declaration
class PrivatePerson extends Person {
    constructor(name, account) {
        super(name, account);
        this._employeesCount = 1;
    }

    get employeesCount() {
        return this._employeesCount;
    }
}

module.exports = PrivatePerson;
