
// Base person declaration
class Person {
    constructor(name, account) {
        // Invariant
        if (account.length >= 8) {
            this._name = name;
            this._account = account;
        } else throw new Error('Invalid initialization!');
    }

    get account() {
        return this._account;
    }

    set account(value) {
        this._account = value;
    }
    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }
}

module.exports = Person;
