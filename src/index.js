const Person = require('./models/persons/private-person');
const Corporation = require('./models/persons/corporation');
const MonthlyPayment = require('./models/payments/monthly-payment');
const AnnualPayment = require('./models/payments/annual-payment');

const person = new Person('Vladislav Vazhoha', '12345678');
const corporation = new Corporation('Tasko', '98745612', 33);

const monthly = new MonthlyPayment(person, 5, 7);
const yearly = new AnnualPayment(corporation, 5);

console.log('Monthly with private person =', monthly.total);
console.log('Yearly with corporation (10 employees) =', yearly.total);

